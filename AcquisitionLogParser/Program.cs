using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq.Expressions;
using Word = Microsoft.Office.Interop.Word;

///// <summary>
///// 
///// </summary>
//public class Acquisition
//{
//    // eine Akquisition kann mehrere Asservate haben
//    /// <summary>
//    /// String representing with which forensic Software the Acquisition was done.
//    /// e.g. Magnet Acquire, AccessData FTK Imager
//    /// </summary>
//    public string SofwareVersion = String.Empty;

//    public string InternalProjectName = String.Empty;

//    /// <summary>
//    /// Date and Time when the acquistion was started
//    /// </summary>
//    public string StartOfAcquisition = String.Empty;

//    /// <summary>
//    /// The date when the acquisition was started
//    /// </summary>
//    public string StartOfAcquisitionWOTime = String.Empty;

//    /// <summary>
//    /// Date and Time when the acquisition was finished
//    /// </summary>
//    public string EndOfAcquisition = String.Empty;

//    /// <summary>
//    /// String representing the name of the hardware write blocker that was used during the acqusition
//    /// </summary>
//    public string NameOfHWWriteBlocker = String.Empty;

//    /// <summary>
//    /// Datastructure to keep track of the evidence objects (Beweismitteldatenträger)
//    /// </summary>
//    public Dictionary<string, EvidenceObject> EvidenceObjects = new Dictionary<string, EvidenceObject>();

//    /// <summary>
//    /// Datastructure to keep track of the evidence images (Beweismitteldatei)
//    /// </summary>
//    public Dictionary<string, EvidenceImage> EvidenceImages = new Dictionary<string, EvidenceImage>();

//}

///// <summary>
///// Asservat
///// </summary>
//public class EvidenceObject
//{
//    // Ein Asservat kann mehrere Datenträger haben 
//    public static string EvidenceIDInternal = String.Empty;
//    public static string EvidenceIDExternal = String.Empty;
//    public static string SerialNumber = String.Empty;

//    public static Dictionary<string, EvidenceStorageMedium> EvidenceStorageMediums = new Dictionary<string, EvidenceStorageMedium>();
//}

///// <summary>
///// e.g ASS01_HDD01
///// </summary>
//public class EvidenceStorageMedium
//{

//    public static string HDDSerialNumber = String.Empty;
//    public static string HDDModel = String.Empty;
//    public static string HDDSizeInGB = String.Empty;
//    public static string HDDSizeInSectors = String.Empty;
//    public static string NumberOfLBABlocks = String.Empty;
//    public static string HDDSizeInBytes = String.Empty;
//    public static string HWInterface = String.Empty;

//    public static string HashSHA1 = String.Empty;
//    public static string HashMD5 = String.Empty;

//    public static string Partition1SizeInBytes = String.Empty;
//    public static string Partition1FileSystem = String.Empty;
//    public static string Partition2SizeInBytes = String.Empty;
//    public static string Partition2FileSystem = String.Empty;
//}

///// <summary>
///// for each EvidenceStorageMedium a forensically sound image is acquired
///// </summary>
//public class EvidenceImage
//{
//    public static string ImageSizeInGB = String.Empty;
//    public static string HashMD5Image = String.Empty;
//    public static string HashSHA1Image = String.Empty;
//}

/// <summary>
/// 
/// </summary>
public class AcquisitionLogFileReader
{

    #region Constants

    public const int FILEREADERROR = -2;
    public const int UNKOWNLOGTYPE = -1;
    public const int TD1LOGTYPE = 1;
    public const int FTKIMAGERLOGTYPE = 2;
    public const int TD3LOGTYPE = 3;
    public const int FTKIMAGERLOGTYPEGERMAN = 4;
    public const int XWAYSFORENSICLOGTYPE = 5;
    public const int TIMLOGTYPE = 6;
    public const int MAGNETACQUIRELOGTYPE = 7;

    public static string Examiner = String.Empty;

    /// <summary>
    /// e.g ASS01_HDD03
    /// </summary>
    public static string EvidenceID = String.Empty;

    /// <summary>
    /// e.g: Asservat 01
    /// </summary>
    public static string EvidenceIDWOHDD = String.Empty;

    /// <summary>
    /// e.g: ASS01
    /// </summary>
    public static string EvidenceIDShort = String.Empty;

    public static string NameOfHWWriteBlocker = String.Empty;
    public static string StartOfAcquisition = String.Empty;
    public static string StartOfAcquisitionWOTime = String.Empty;
    public static string EndOfAcquisition = String.Empty;
    public static string HDDSerialNumber = String.Empty;
    public static string HDDModel = String.Empty;
    public static string HDDSizeInGB = String.Empty;
    public static string HDDSizeInSectors = String.Empty;
    public static string NumberOfLBABlocks = String.Empty;
    public static string HDDSizeInBytes = String.Empty;
    public static string HWInterface = String.Empty;

    public static string HashSHA1 = String.Empty;
    public static string HashMD5 = String.Empty;
    public static string HashMD5Image = String.Empty;
    public static string HashSHA1Image = String.Empty;

    public static string LogFileContent = String.Empty;
    public static string ImageSizeInGB = String.Empty;
    public static string SofwareVersion = String.Empty;

    public static string Partition1SizeInBytes = String.Empty;
    public static string Partition1FileSystem = String.Empty;
    public static string Partition2SizeInBytes = String.Empty;
    public static string Partition2FileSystem = String.Empty;

    public static string InternalProjectName = String.Empty;

    #endregion

    /// <summary>
    /// New: This Method determins the type of the log by investigating all lines of the file and searching for patterns.
    /// Deprecated: This Method determines the type of log by investigating the first line of the file.
    /// </summary>
    /// <param name="fLogLine">first line of the log file to read in</param>
    /// <returns>
    /// returns the corresponding constant values
    /// </returns>
    public static int determineLogType(string _logFileContent, string _path)
    {
        // early out
        if (_logFileContent.Length == 0)
        {
            Console.WriteLine("Error opening or file. File maybe empty?");
            return AcquisitionLogFileReader.FILEREADERROR;
        }

        // TABLEAU Imager Log does not have any characteristic line 
        // from which the log type could be determined, thus we need to 
        // pray that Exaimers save this log file as ASS01_HDD01_TIM.txt
        string[] partOfFilePath = _path.Split('_');
        if (partOfFilePath[partOfFilePath.Length - 1].IndexOf("TIM") != -1)
        {
            Console.WriteLine("Identified Tableau Imager Log File");
            return AcquisitionLogFileReader.TIMLOGTYPE;
        }

        // go through all lines of the file;
        string[] lines = _logFileContent.Split('\n');
        foreach (string _fLogLine in lines)
        {
            // for every known log file format review the first line in order to determine the type of log file
            if (_fLogLine.IndexOf("Start of Tableau TD1 Log entry", StringComparison.CurrentCultureIgnoreCase) != -1
                && _fLogLine.IndexOf("Start of Tableau TD1 Log entry", StringComparison.CurrentCultureIgnoreCase) != 0)
            {
                Console.WriteLine("identified TD1 Logfile");
                return AcquisitionLogFileReader.TD1LOGTYPE;
            }

            if (_fLogLine.IndexOf("Start of TD3 Log Entry", StringComparison.CurrentCultureIgnoreCase) != -1
                && _fLogLine.IndexOf("Start of TD3 Log Entry", StringComparison.CurrentCultureIgnoreCase) != 0)
            {
                Console.WriteLine("identified TD3 Logfile");
                return AcquisitionLogFileReader.TD3LOGTYPE;
            }

            // AccessData
            if (_fLogLine.IndexOf("Created By AccessData", StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                Console.WriteLine("identified FTK Imager Logfile");
                return AcquisitionLogFileReader.FTKIMAGERLOGTYPE;
            }

            if (_fLogLine.IndexOf("Erstellt von AccessData", StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                Console.WriteLine("identified FTK Imager Logfile in German language");
                return AcquisitionLogFileReader.FTKIMAGERLOGTYPEGERMAN;
            }

            // X-Ways Log
            if (_fLogLine.IndexOf("X-Ways Forensics", StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                Console.WriteLine("identified X-Ways Forensic Logfile");
                return AcquisitionLogFileReader.XWAYSFORENSICLOGTYPE;
            }

            // Magnet Acquire
            if (_fLogLine.IndexOf("Magnet ACQUIRE", StringComparison.CurrentCultureIgnoreCase) != -1
                && _fLogLine.IndexOf("Magnet ACQUIRE", StringComparison.CurrentCultureIgnoreCase) != 0)
            {
                Console.WriteLine("identified Magnet Acquire Logfile");
                return AcquisitionLogFileReader.MAGNETACQUIRELOGTYPE;
            }

        }

        // if we end up here return errorcode
        return AcquisitionLogFileReader.UNKOWNLOGTYPE;
    }

    /// <summary>
    /// Date converter
    /// </summary>
    /// <param name="_dateString">the date as string parsed from log file</param>
    /// <param name="_germanDate">wheter or not to change to german date format</param>
    /// <returns></returns>
    public static string ConvertDate(string _dateString, bool _germanDate)
    {
        DateTime test = Convert.ToDateTime(_dateString);
        string date = "";
        if (_germanDate)
        {
            //convert to german date format
            date = (test.Day.ToString().PadLeft(2, '0') + "." +
                test.Month.ToString().PadLeft(2, '0') + "." +
                test.Year + " " +
                test.Hour.ToString().PadLeft(2, '0') + ":" +
                test.Minute.ToString().PadLeft(2, '0') +
                " Uhr"
                );
        }
        return date;
    }

    /// <summary>
    /// Splits the hash into chuncks of 4 characters.
    /// </summary>
    /// <param name="_hash"></param>
    /// <returns>Hash as string with a space every 4 charachters</returns>
    public static string GetFormatedHash(string _hash)
    {
        //Console.WriteLine("_hash[0]=" + _hash[0]);
        string temp = String.Empty;

        temp += _hash[0];
        for (int i = 1; i < _hash.Length; i++)
        {
            if (i % 4 == 0)
            {
                temp += " " + _hash[i];
            }
            else
                temp += _hash[i];
        }
        return temp;
    }

    private static string GetEvidenceIDShortFormat(string _evidenceID)
    {
        return EvidenceIDShort = _evidenceID.Split('_')[0];
    }

    private static string GetEvidenceIDWithoutHDD(string _evidenceID)
    {
        string _evidenceName = "Asservat ";
        int _evidenceNumber = Int32.Parse(_evidenceID.Split('_')[0].Substring(_evidenceID.Split('_')[0].Length - 1));
        if (_evidenceNumber <= 9)
        {
            _evidenceName += "0";
        }
        return _evidenceName + _evidenceNumber.ToString();
    }

    private static string GetNumberOfLBABlocks(string _hddSizeInSectors, int _bytesPerSector)
    {
        // remove unwanted characters in order to be able to parse it to integers ;)
        if (_hddSizeInSectors.Contains("."))
        {
            _hddSizeInSectors = RemoveCharsFromString(_hddSizeInSectors, '.');
        }
        // number of LBA blocks
        try
        {
            UInt64 hddSectors = UInt64.Parse(_hddSizeInSectors);
            //Console.WriteLine(hddSectors);

            UInt64 lba = hddSectors / 512;
            //Console.WriteLine(lba);
            return lba.ToString();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return "n/a";
        }
    }

    private static string GetHDDSizeInBytesFromSectorCount(string _hddSizeInSectors, int _bytesPerSector)
    {
        // remove unwanted characters in order to be able to parse it to integers ;)
        if (_hddSizeInSectors.Contains("."))
        {
            _hddSizeInSectors = RemoveCharsFromString(_hddSizeInSectors, '.');
        }
        // number of LBA blocks
        try
        {
            UInt64 hddSectors = UInt64.Parse(_hddSizeInSectors);
            //Console.WriteLine(hddSectors);

            UInt64 hddBytes = hddSectors * (UInt64)_bytesPerSector;
            //Console.WriteLine(lba);
            return hddBytes.ToString();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return "n/a";
        }
    }

    private static string RemoveCharsFromString(string _input, char _charToRemove)
    {
        String temp = String.Empty;
        for (int i = 0; i < _input.Length; i++)
        {
            if (_input[i] != _charToRemove)
            {
                temp += _input[i];
            }
        }
        return temp;
    }

    private static void parseTD1Log(string _logFileContent)
    {

        // vars used
        int idx = 0;
        string date = String.Empty;
        string findString = String.Empty;

        bool isSourceDrive = true;
        NameOfHWWriteBlocker = "TD 1";

        // go through the text file
        string[] lines = _logFileContent.Split('\n');

        // optional output
        Console.WriteLine("parsing " + lines.Length + " lines of TD1LOGFILE");

        foreach (string line in lines)
        {
            // find User
            findString = "User:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);
                // User: Tableau 
                Examiner = line.Substring(idx + (findString.Length), (line.Length - findString.Length)).Trim();
                Console.WriteLine("Exaimer: " + Examiner);
            }

            // determine Image size in GB
            findString = "Chunk size in sectors:";
            if (line.IndexOf(findString) != -1)
            {
                // Chunk size in sectors: 7,812,480 (3.9 GB)
                idx = line.IndexOf(findString);

                int tmp = line.IndexOf("(");
                ImageSizeInGB = line.Substring((tmp + 1), ((line.Length - 1) - (tmp + 2))).Trim();
                Console.WriteLine("ImageSizeInGB: " + ImageSizeInGB);
            }

            // find date of acuisition
            findString = "Created:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);
                date = line.Substring(idx + (findString.Length + 1), 16);

                StartOfAcquisition = ConvertDate(date, true).Trim();
                Console.WriteLine("Datensicherung: " + StartOfAcquisition);
            }

            // find end of acquisition
            findString = "Closed :";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);
                date = line.Substring(idx + (findString.Length + 1), 16);

                EndOfAcquisition = ConvertDate(date, true).Trim();
                Console.WriteLine("Ende der Datensicherung: " + EndOfAcquisition);
            }

            // find S/N of source drive
            findString = "S/N:";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                idx = line.IndexOf(findString);
                HDDSerialNumber = line.Substring((idx + (findString.Length + 1))).Trim();

                Console.WriteLine("S/N der gesicherten Festplatte: " + HDDSerialNumber);
            }

            // find Interface type
            findString = "Cable/Interface type:";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                // do not read the serial number and interface type of the target drive :)
                isSourceDrive = false;
                idx = line.IndexOf(findString);
                HWInterface = line.Substring(idx + (findString.Length)).Trim();
                Console.WriteLine("Anschluss/Interface: " + HWInterface);
            }

            // find hashsum SHA-1
            findString = "SHA1";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                idx = line.IndexOf(findString);
                HashSHA1 = GetFormatedHash(line.Substring(idx + (findString.Length + 1)).Trim());

                Console.WriteLine("Prüfsumme (SHA-1): " + HashSHA1);
            }

            // find hashsum MD5
            findString = "MD5";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                idx = line.IndexOf(findString);
                // its not MD5: ... its MD5 : this needs to be checked...
                HashMD5 = GetFormatedHash(line.Substring(idx + (findString.Length + 2)).Trim());
                Console.WriteLine("Prüfsumme (MD5): " + HashMD5);
            }

            // find HDD Model
            findString = "Model:";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                idx = line.IndexOf(findString);
                HDDModel = line.Substring((idx + (findString.Length + 1))).Trim();
                Console.WriteLine("HDD Model: " + HDDModel);
            }

            // find size in GB and size in sectors
            findString = "# of sectors:";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                idx = line.IndexOf(findString);

                int tmp = line.IndexOf("(");
                // actually had to add two chars, because of spaces before line feed -.-
                HDDSizeInGB = line.Substring((tmp + 1), ((line.Length - 1) - (tmp + 2))).Trim();
                HDDSizeInSectors = line.Substring((idx + (findString.Length + 1)), line.Length - tmp).Replace(',', '.').Trim();
                Console.WriteLine("Anzahl von Sektoren: " + HDDSizeInSectors);
                Console.WriteLine("Größe in GB: " + HDDSizeInGB);

                String tempSize = String.Empty;
                for (int i = 0; i < HDDSizeInSectors.Length; i++)
                {
                    if (HDDSizeInSectors[i] != '.')
                    {
                        tempSize += HDDSizeInSectors[i];
                    }
                }

                NumberOfLBABlocks = GetNumberOfLBABlocks(tempSize, 512);
                Console.WriteLine("Anzahl der LBA Blöcke: " + NumberOfLBABlocks);

                UInt64 sizeInSectors = 0;
                if (UInt64.TryParse(tempSize, out sizeInSectors))
                {
                    //Console.WriteLine("sizeInSectors: " + sizeInSectors);
                    UInt64 temp = sizeInSectors * 512;
                    //Console.WriteLine("sizeInSectors * 512: " + temp);
                    HDDSizeInBytes = temp.ToString();
                    Console.WriteLine("Größe in Bytes: " + HDDSizeInBytes);
                }
                else
                {
                    Console.WriteLine("Error determining HDDSizeInBytes");
                    HDDSizeInBytes = "0";
                }

            }
        }
    }

    private static void parseTD3Log(string _logFileContent)
    {
        // vars used
        int idx = 0;
        string date = String.Empty;
        string findString = String.Empty;
        bool isSourceDrive = true;
        char _delimiter = ':';

        NameOfHWWriteBlocker = "TD 3";

        // go through the text file
        string[] lines = _logFileContent.Split('\n');

        // optional output
        Console.WriteLine("parsing " + lines.Length + " lines of TD3LOGFILE");

        foreach (string line in lines)
        {
            // find User
            findString = "User:"; // User: Tableau 
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out Examiner);
                Console.WriteLine("Exaimer: " + Examiner);
            }

            // determine Image size in GB
            findString = "Chunk size in sectors:";
            if (line.IndexOf(findString) != -1)
            {
                // Chunk size in sectors: 7,812,480 (3.9 GB)
                idx = line.IndexOf(findString);

                int tmp = line.IndexOf("(");
                ImageSizeInGB = line.Substring((tmp + 1), ((line.Length - 1) - (tmp + 2))).Trim();
                Console.WriteLine("ImageSizeInGB: " + ImageSizeInGB);
            }

            // find date of acquisition
            findString = "Created:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);
                date = correctDateString(line.Substring(idx + (findString.Length + 1)).Trim());

                StartOfAcquisition = ConvertDate(date, true).Trim();
                Console.WriteLine("Datensicherung: " + StartOfAcquisition);
            }

            // find end of acquisition
            findString = "Closed:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);
                date = correctDateString(line.Substring(idx + (findString.Length + 1)).Trim());

                EndOfAcquisition = ConvertDate(date, true).Trim();
                Console.WriteLine("Ende der Datensicherung: " + EndOfAcquisition);
            }

            // find Interface type
            findString = "Interface: ";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HWInterface);
                Console.WriteLine("Anschluss/Interface: " + HWInterface);
            }

            // find HDD Model
            findString = "Model:";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HDDModel);
                Console.WriteLine("HDD Model: " + HDDModel);
            }

            // find S/N of source drive
            findString = "Serial number:";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HDDSerialNumber);
                //idx = line.IndexOf(findString);
                //HDDSerialNumber = line.Substring((idx + (findString.Length + 1))).Trim();
                Console.WriteLine("S/N der gesicherten Festplatte: " + HDDSerialNumber);
            }

            // HDDSizeInGB && HDDSizeInSectors && HDDSizeInBytes
            findString = "Capacity in bytes:";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                int tmp = line.IndexOf("(");
                // Capacity in bytes: 250,059,350,016 (250.0 GB)
                HDDSizeInGB = line.Substring((tmp + 1), ((line.Length - 1) - (tmp + 2))).Trim();

                int start = (findString.Length + 1);
                int end = (tmp);
                int len = (end - start);
                /*Console.WriteLine(start);
                Console.WriteLine(end);
                Console.WriteLine(len);
                Console.WriteLine(line.Length);*/

                HDDSizeInBytes = line.Substring(start, len);
                HDDSizeInSectors = line.Substring(start, len).Replace(',', '.').Trim();

                Console.WriteLine("Anzahl von Sektoren: " + HDDSizeInSectors);
                Console.WriteLine("Größe in GB: " + HDDSizeInGB);
                Console.WriteLine("Größe in Bytes: " + HDDSizeInBytes);
            }

            // find size in LBA Blocks
            findString = "Block Count:";
            if (line.IndexOf(findString) != -1 && isSourceDrive)
            {
                // block count is the last information we need form the source drive
                isSourceDrive = false;
                string blocks = String.Empty;

                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out blocks);
                //idx = line.IndexOf(findString);
                //string blocks = line.Substring(idx + findString.Length).Trim();

                // remove the ,
                NumberOfLBABlocks = RemoveCharsFromString(blocks, ',');

                // we dont need this function is this log type because the number of blocks is parsable (GetNumberOfLBABlocks(tempSize, 512));
                Console.WriteLine("Anzahl der LBA Blöcke: " + NumberOfLBABlocks);
            }

            // find hashsum SHA-1
            findString = "Acquisition: SHA1";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HashSHA1);
                Console.WriteLine("Prüfsumme des Beweismitteldatenträgers (SHA-1): " + HashSHA1);
            }

            // find hashsum MD5
            findString = "Acquisition MD5:";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HashMD5);
                Console.WriteLine("Prüfsumme des Beweismitteldatenträgers (MD5): " + HashMD5);
            }

            // find hashsum SHA-1
            findString = "Verification SHA - 1:";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HashSHA1Image);
                Console.WriteLine("Prüfsumme der Verifikation (SHA-1): " + HashSHA1Image);
            }

            // find hashsum MD5
            findString = "Verification MD5:";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HashMD5Image);
                Console.WriteLine("Prüfsumme der Verifikation (MD5): " + HashMD5Image);
            }

        }
        if (HashMD5 == HashMD5Image && HashSHA1 == HashSHA1Image)
        {
            Console.WriteLine("Erfolg: Prüfsumme des Beweissmitteldatenträgers und der forensisch erhobenen Beweismitteldatei stimmen überein");
        }
        else
        {
            Console.WriteLine("Keine Verification der Hashwerte erfolgt oder diese war nicht erfolgreich");
        }
    }

    private static void parseTIMLog(string _logFileContent)
    {
        // vars used
        //int idx = 0;
        string date = String.Empty;
        string findString = String.Empty;

        // cant determine the hw write blocker out of this logfile.
        // should always be written down in the logfile so it could be parsed u know ;)
        NameOfHWWriteBlocker = "n/a";
    }


    /* public static class MemberInfoGetting
     {
         public static string GetMemberName<T>(Expression<Func<T>> memberExpression)
         {
             MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
             return expressionBody.Member.Name;
         }
     }*/
    //To get name of a variable:

    //string testVariable = "value";
    //string nameOfTestVariable = MemberInfoGetting.GetMemberName(() => testVariable);

    // out bewirkt das das ergebnis in diese variable geschrieben wird

    /// <summary>
    /// highly generalized method to extract easy cases of information from several log formats
    /// caution: black magicx
    /// </summary>
    /// <param name="_keyword">findString</param>
    /// <param name="_line">the current line of the log file</param>
    /// <param name="_information">a parameter/string that gets filled with data</param>
    private static void extractInformationFromKeywordUsingDelimter(string _keyword, string _line, char _delimiter, out string _information)
    {
        _information = _line.Split(_delimiter)[1].Trim();
    }

    /// <summary>
    /// This log file isnt that great; no verification of the hashes ... etc.
    /// </summary>
    /// <param name="_logFileContent"></param>
    private static void parseMagnetAcquireLog(string _logFileContent)
    {
        // vars used
        //int idx = 0;
        string date = String.Empty;
        string findString = String.Empty;
        char _delimiter = ':';

        // cant determine the hw write blocker out of this logfile.
        // should always be written down in the logfile so it could be parsed u know ;)
        NameOfHWWriteBlocker = "n/a";

        // go through the text file
        string[] lines = _logFileContent.Split('\n');

        // optional output
        Console.WriteLine("parsing " + lines.Length + " lines of Magnet Acquire Logfile");

        // Determine SofwareVersion
        SofwareVersion = lines[0].Split(':')[1].Trim() + lines[1].Split(':')[1].TrimEnd();
        Console.WriteLine("SoftwareVersion:" + SofwareVersion);

        for (int i = 1; i < lines.Length; i++)
        {

            #region GeneralInformation
            findString = "Examiner Name:";
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out Examiner);
            }

            findString = "Evidence Number:";
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out EvidenceID);
                EvidenceIDShort = GetEvidenceIDShortFormat(EvidenceID);
                EvidenceIDWOHDD = GetEvidenceIDWithoutHDD(EvidenceID);
            }
            #endregion

            findString = "Product Model:";
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out HDDModel);
            }

            #region EvidenceStorageMedium
            // this one has to be observed since the same keyword 
            // is used for giving the total number of sectors for the image
            // but as of right now the Magnet Acquire only does raw/dd images the amount should be the same!?
            // with the second condition it should always find the correct one tho ;)
            findString = "Total Sectors:";
            if (lines[i].IndexOf(findString) != -1 && lines[i].IndexOf(findString) == 0)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out HDDSizeInSectors);
            }

            // our guess is that this one actually is the HDDSizeInBytes
            findString = "Size:";
            if (lines[i].IndexOf(findString) != -1 && lines[i].IndexOf(findString) == 0)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out HDDSizeInBytes);
            }
            findString = "Device Size:";
            if (lines[i].IndexOf(findString) != -1 && lines[i].IndexOf(findString) == 0)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out HDDSizeInGB);
            }

            // this should be the last thing
            findString = "Partitions";
            if (lines[i].IndexOf(findString) != -1 && lines[i].IndexOf(findString) == 0)
            {
                Console.WriteLine("Determining Partition Information:");
                for (int j = i + 1; j < lines.Length - 1; j++)
                {
                    string PartitionNumber = (lines[j].Split(_delimiter)[0]).Trim();
                    Console.WriteLine(PartitionNumber);
                    //string temp = lines[j].Split('(')[1];
                    //string PartitionFileSystem = temp.Substring(0, temp.Length - 2);
                    //Console.WriteLine("PartitionFileSystem: " + PartitionFileSystem);
                    // lines[j]
                    /*Partition1FileSystem;
                    Partition1SizeInBytes;
                    Partition2FileSystem;
                    Partition2SizeInBytes;*/
                }


            }
            #endregion

            #region ImageHashValues
            // it seems like there is no verification of the hash values done by magnet acquire
            findString = "Segment 1 MD5 Hash:";
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out HashMD5Image);
            }

            findString = "Segment 1 SHA1 Hash:";
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out HashSHA1Image);
            }
            #endregion

        } // end for loop
    }

    /// <summary>
    /// FTK Imager date does not comply to standard date formats, this method corrects the date string format.
    /// </summary>
    /// <param name="_dateString"></param>
    /// <returns></returns>
    private static string correctDateString(string _dateString)
    {
        // The DateTime Format given in the FTK Imager Logfile is close to one 
        // that could be converted via Convert.ToDateTime(String);
        // thats why we alter the string to fit the format used to convert.
        // the needed format is = "Sat, 10 May 2008 14:32:17 GMT";

        char _space = ' ';
        String[] _datecomponents = _dateString.Split(_space);

        // [0],[1],[2],[3],    [4]
        // Fri Jun 12 07:49:55 2009
        // needed format
        // Sat, 10 May 2008 14:32:17 GMT
        // => [0]+",", [2], [1], [4], [3]+" GMT"

        return _datecomponents[0] + ", "
            + _datecomponents[2]
            + _space + _datecomponents[1]
            + _space + _datecomponents[4]
            + _space + _datecomponents[3]
            + " GMT";
    }

    private static void parseXWAYSLog(string _logFileContent)
    {
        // vars used
        int idx = 0;
        string date = String.Empty;
        string findString = String.Empty;
        char _delimiter = ':';

        // cant determine the hw write blocker out of this logfile.
        // should always be written down in the logfile so it could be parsed u know ;)
        NameOfHWWriteBlocker = "n/a";

        // go through the text file
        string[] lines = _logFileContent.Split('\n');

        // optional output
        Console.WriteLine("parsing " + lines.Length + " lines of X-Ways Forensics LogFile");

        // simple cases first (16.06.2016, 11:47:15)
        StartOfAcquisition = lines[0];
        StartOfAcquisitionWOTime = lines[0].Split(',')[0];

        // Software Version
        SofwareVersion = lines[1];

        for (int i = 0; i < lines.Length; i++)
        {
            // simple cases first;

            // find User
            findString = "Bearbeiter:";
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out Examiner);
                Console.WriteLine("Examiner: " + Examiner);
            }

            // find hashsum MD5
            findString = "Hash der Quelldaten:";
            if (lines[i].IndexOf(findString) != -1)
            {
                //"Hash der Quelldaten: BBD471BA54D83266350F2E7D86A31778 (MD5) "
                idx = lines[i].IndexOf(findString);
                HashMD5 = GetFormatedHash(lines[i].Substring(idx + (findString.Length), (lines[i].Length - (findString.Length + 6))).Trim());
                Console.WriteLine("Prüfsumme (MD5): " + HashMD5);
            }

            //ImageSizeInGb
            findString = "Sicherung abgeschlossen: ";// 162 GB
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out ImageSizeInGB);
                Console.WriteLine("Image size in GB: " + ImageSizeInGB);
            }

            //HWInterface
            findString = "Bus: ";
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out HWInterface);
                Console.WriteLine("HWInterface: " + HWInterface);
            }

            // HDDModel
            findString = "Modell:";
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out HDDModel);
                Console.WriteLine("Drive Model: " + HDDModel);
            }
            // HDDSerialNumber
            findString = "Seriennr.:";
            if (lines[i].IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, lines[i], _delimiter, out HDDSerialNumber);
                Console.WriteLine("HDD Serialnumber: " + HDDSerialNumber);
            }

            // HDDSizeInGB
            findString = "Gesamtkapazität: ";
            if (lines[i].IndexOf(findString) != -1)
            {
                // Gesamtkapazität: 250.059.350.016 Bytes = 233 GB
                extractInformationFromKeywordUsingDelimter(findString, lines[i], '=', out HDDSizeInGB);
                Console.WriteLine("HDDSizeInGB: " + HDDSizeInGB);
            }

            // more complex cases, since the log format is not the best to be fair

            // find information regarding source HDD
            if (lines[i].IndexOf("Quelle:") != -1)
            {
                Console.WriteLine("determining source HDD infos");
                //search the next couple of lines
                for (int j = i + 1; j <= (i + 2); j++)
                {

                    // HDDSizeInSectors; number of sectors
                    if (lines[j].IndexOf("Sektoren ") != -1)
                    {
                        findString = "-";
                        idx = lines[j].IndexOf(findString);
                        //Sektoren 0 - 488397167
                        HDDSizeInSectors = lines[j].Substring((idx + 1), (lines[j].Length - (idx + 1))).Trim();
                        Console.WriteLine("HDDSizeInSectors: " + HDDSizeInSectors);

                        NumberOfLBABlocks = GetNumberOfLBABlocks(HDDSizeInSectors, 512);
                        Console.WriteLine("Anzahl der LBA Blöcke: " + NumberOfLBABlocks);
                    }

                    // EvidenceID
                    if (lines[j].IndexOf("Ziel:") != -1)
                    {
                        string[] partsOfFilePath = lines[j].Split('\\');

                        // substring of the last part of the path and remove 4 chars to remove .e01 ;)
                        EvidenceID = partsOfFilePath[partsOfFilePath.Length - 1].Substring(0, (partsOfFilePath[partsOfFilePath.Length - 1]).Length - 5);
                        Console.WriteLine("EvidenceID: " + EvidenceID);

                        EvidenceIDShort = GetEvidenceIDShortFormat(EvidenceID);
                        EvidenceIDWOHDD = GetEvidenceIDWithoutHDD(EvidenceID);

                        // InternalProjectName
                        InternalProjectName = partsOfFilePath[partsOfFilePath.Length - 2];
                    }
                }
            }

            // complex case
            if (lines[i].IndexOf("Partition 1") != -1)
            {
                // determine Partition Size
                findString = "Sektoren ";
                if (lines[i + 1].IndexOf("Sektoren ") != -1)
                {
                    Console.WriteLine("Determining Partition 1 size");
                    string tmp = lines[i + 1].Substring((findString.Length)).Trim();
                    Console.WriteLine(tmp);
                    string[] sectors = tmp.Split('-');
                    UInt64 startSector = UInt64.Parse(sectors[0].Replace(".", string.Empty).Trim());
                    //Console.WriteLine(sectors[0].Trim());
                    //Console.WriteLine(sectors[1].Replace(".", string.Empty).Trim());
                    UInt64 endSector = UInt64.Parse(sectors[1].Replace(".", string.Empty).Trim());

                    UInt64 numSectors = endSector - startSector;
                    UInt64 partSizeInBytes = numSectors * 512;
                    Partition1SizeInBytes = (numSectors * 512).ToString();

                    Console.WriteLine("Number of Sectors: " + numSectors.ToString());
                    Console.WriteLine("Partition 1 Size in Bytes: " + Partition1SizeInBytes);

                }
                if (lines[i + 3].Length >= 1)
                {
                    Partition1FileSystem = lines[i + 3];
                    Console.WriteLine("Partition 1 Filesystem: " + Partition1FileSystem);
                }
            }


            if (lines[i].IndexOf("Partition 2") != -1)
            {
                // determine Partition Size
                findString = "Sektoren ";
                if (lines[i + 1].IndexOf("Sektoren ") != -1)
                {
                    Console.WriteLine("Determining Partition 2 size");
                    string tmp = lines[i + 1].Substring(idx + (findString.Length)).Trim();
                    Console.WriteLine(tmp);
                    string[] sectors = tmp.Split('-');
                    UInt64 startSector = UInt64.Parse(sectors[0].Replace(".", string.Empty).Trim());
                    UInt64 endSector = UInt64.Parse(sectors[1].Replace(".", string.Empty).Trim());

                    UInt64 numSectors = endSector - startSector;
                    //UInt64 partSizeInBytes = numSectors * 512;
                    Partition2SizeInBytes = (numSectors * 512).ToString();

                    Console.WriteLine("Number of Sectors: " + numSectors.ToString());
                    Console.WriteLine("Partition 2 Size in Bytes: " + Partition2SizeInBytes);

                }
                if (lines[i + 3].Length >= 1)
                {
                    Partition2FileSystem = lines[i + 3];
                    Console.WriteLine("Partition 2 Filesystem: " + Partition2FileSystem);
                }
            }


        }
    }

    private static void parseFTKGermanLog(string _logFileContent)
    {
        // vars used
        int idx = 0;
        string date = String.Empty;
        string findString = String.Empty;
        char _delimiter = ':';

        // cant determine the hw write blocker out of this logfile.
        // should always be written down in the logfile so it could be parsed u know ;)
        NameOfHWWriteBlocker = "n/a";

        // go through the text file
        string[] lines = _logFileContent.Split('\n');

        // optional output
        Console.WriteLine("parsing " + lines.Length + " lines of FTK IMAGER LOGFILE");

        // determine Software Version ( Length - 2 because the last field is just a space or something )
        SofwareVersion = "Access Data FTK Imager " + lines[0].Split(' ')[lines[0].Split(' ').Length - 2];
        Console.WriteLine("Software Version: " + SofwareVersion);

        foreach (string line in lines)
        {
            // find User
            findString = "Prüfer:"; // Prüfer: Tableau 
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out Examiner);
                Console.WriteLine("Exaimer: " + Examiner);
            }

            // Internal Project Name
            //Fallnummer: abc def
            findString = "Fallnummer:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);
                InternalProjectName = line.Substring(idx + (findString.Length), (line.Length - findString.Length)).Trim();
                Console.WriteLine("Internal Project Name: " + InternalProjectName);
            }
            // EvidenceNumber
            findString = "Beweisnummer:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);

                EvidenceID = line.Substring(idx + (findString.Length), (line.Length - findString.Length)).Trim();
                Console.WriteLine("EvidenceID: " + EvidenceID);

                // get short format 
                EvidenceIDShort = GetEvidenceIDShortFormat(EvidenceID);

                // get format without HDD
                EvidenceIDWOHDD = GetEvidenceIDWithoutHDD(EvidenceID);

            }

            // find date of acuisition
            findString = "Acquisition started:";
            if (line.IndexOf(findString) != -1)
            {
                // Console.WriteLine(line);
                idx = line.IndexOf(findString);
                date = line.Substring(idx + (findString.Length + 1)).Trim();

                // correct the date string
                date = correctDateString(date);

                StartOfAcquisition = ConvertDate(date, true);
                Console.WriteLine("Start der Datensicherung: " + StartOfAcquisition);

                StartOfAcquisitionWOTime = StartOfAcquisition.Split(' ')[0];
                Console.WriteLine("Datum der Datensicherung: " + StartOfAcquisitionWOTime);
            }

            // find end of acquisition
            findString = "Acquisition finished:";
            if (line.IndexOf("Acquisition finished:") != -1)
            {
                idx = line.IndexOf(findString);
                date = line.Substring(idx + (findString.Length + 1)).Trim();

                char _space = ' ';
                String[] _datecomponents = date.Split(_space);
                _datecomponents[0] = _datecomponents[0] + ", ";

                // correct the date string
                date = correctDateString(date);

                EndOfAcquisition = ConvertDate(date, true);
                Console.WriteLine("Ende der Datensicherung: " + EndOfAcquisition);
            }

            // find S/N of source drive
            findString = "Seriennummer des Laufwerks:";
            if (line.IndexOf(findString) != -1)// && isSourceDrive)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HDDSerialNumber);
                Console.WriteLine("S/N der gesicherten Festplatte: " + HDDSerialNumber);
            }

            // find HDD Interface type
            findString = "Schnittstellentyp des Laufwerks:";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HWInterface);
                Console.WriteLine("Anschluss/Interface: " + HWInterface);
            }

            // find hashsum SHA-1
            findString = "SHA1 checksum:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);

                // we only need one hash so skip the second one
                if (line.Contains("verified"))
                {
                    HashSHA1 = GetFormatedHash(line.Split(_delimiter)[1].Trim());
                    Console.WriteLine("Prüfsumme des Beweismitteldatenträgers (SHA1): " + HashSHA1);
                }
                else
                {
                    extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HashSHA1Image);
                    HashSHA1Image = GetFormatedHash(HashSHA1Image);
                    Console.WriteLine("Prüfsumme der Beweismitteldatei(SHA-1): " + HashSHA1Image);
                }
            }

            // find hashsum MD5
            findString = "MD5 checksum:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);

                // we only need one hash so skip the second one
                if (line.Contains("verified"))
                {
                    HashMD5 = GetFormatedHash(line.Split(_delimiter)[1].Trim());
                    Console.WriteLine("Prüfsumme des Beweismitteldatenträgers (MD5): " + HashMD5);
                }
                else
                {
                    extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HashMD5Image);
                    Console.WriteLine("Prüfsumme der Beweismitteldatei (MD5): " + HashMD5Image);
                }
            }

            // find HDD Model 
            findString = "Laufwerksmodell:";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HDDModel);
                Console.WriteLine("Drive Model: " + HDDModel);
            }

            // sector count and LBA blocks
            findString = "Sector count:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);

                HDDSizeInSectors = line.Substring(idx + (findString.Length + 1)).Replace(',', '.').Trim();
                Console.WriteLine("Anzahl von Sektoren: " + HDDSizeInSectors);

                NumberOfLBABlocks = GetNumberOfLBABlocks(HDDSizeInSectors, 512);
                Console.WriteLine("Anzahl der LBA Blöcke: " + NumberOfLBABlocks);
            }

            // find size in GB 
            findString = "Source data size";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HDDSizeInGB);
                Console.WriteLine("Größe in GB: " + HDDSizeInGB);
            }

        }
    }

    private static void parseFTKLog(string _logFileContent)
    {
        // vars used
        int idx = 0;
        string date = String.Empty;
        string findString = String.Empty;
        char _delimiter = ':';

        // cant determine the hw write blocker out of this logfile.
        // should always be written down in the logfile so it could be parsed u know ;)
        NameOfHWWriteBlocker = "n/a";

        // go through the text file
        string[] lines = _logFileContent.Split('\n');

        // optional output
        Console.WriteLine("parsing " + lines.Length + " lines of FTK IMAGER LOGFILE");

        // determine Software Version ( Length - 2 because the last field is just a space or something )
        SofwareVersion = "Access Data FTK Imager " + lines[0].Split(' ')[lines[0].Split(' ').Length - 2];
        Console.WriteLine("Software Version: " + SofwareVersion);

        foreach (string line in lines)
        {
            // find date of acuisition
            findString = "Acquisition started:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);
                date = line.Substring(idx + (findString.Length + 1)).Trim();

                // correct the date string
                date = correctDateString(date);

                StartOfAcquisition = ConvertDate(date, true);
                Console.WriteLine("Datensicherung: " + StartOfAcquisition);
                StartOfAcquisitionWOTime = StartOfAcquisition.Split(' ')[0];
                Console.WriteLine("Datum der Datensicherung: " + StartOfAcquisitionWOTime);
            }

            // find end of acquisition
            findString = "Acquisition finished:";
            if (line.IndexOf("Acquisition finished:") != -1)
            {
                idx = line.IndexOf(findString);
                date = line.Substring(idx + (findString.Length + 1)).Trim();

                char _space = ' ';
                String[] _datecomponents = date.Split(_space);
                _datecomponents[0] = _datecomponents[0] + ", ";

                // correct the date string
                date = correctDateString(date);

                EndOfAcquisition = ConvertDate(date, true);
                Console.WriteLine("Ende der Datensicherung: " + EndOfAcquisition);
            }

            // find S/N of source drive
            findString = "Drive Serial Number:";
            if (line.IndexOf(findString) != -1)// && isSourceDrive)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HDDSerialNumber);
                Console.WriteLine("S/N der gesicherten Festplatte: " + HDDSerialNumber);
            }

            // find HDD Interface type
            findString = "Drive Interface Type:";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HWInterface);
                Console.WriteLine("Anschluss/Interface: " + HWInterface);
            }

            // find hashsum SHA-1
            findString = "SHA1 checksum:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);

                // we only need one hash so skip the second one
                if (line.Contains("verified"))
                {
                    HashSHA1 = GetFormatedHash(line.Split(_delimiter)[1].Trim());
                    Console.WriteLine("Prüfsumme des Beweismitteldatenträgers (SHA-1): " + HashSHA1);
                }
                else
                {
                    extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HashSHA1Image);
                    HashSHA1Image = GetFormatedHash(HashSHA1Image);
                    Console.WriteLine("Prüfsumme der Beweismitteldatei (SHA-1): " + HashSHA1Image);
                }
            }

            // find hashsum MD5
            findString = "MD5 checksum:";
            if (line.IndexOf(findString) != -1)
            {
                idx = line.IndexOf(findString);

                // we only need one hash so skip the second one
                if (line.Contains("verified"))
                {
                    HashMD5 = GetFormatedHash(line.Split(_delimiter)[1].Trim());
                    Console.WriteLine("Prüfsumme des Beweismitteldatenträgers (MD5): " + HashMD5);
                }
                else
                {
                    extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HashMD5Image);
                    HashMD5Image = GetFormatedHash(HashMD5Image);
                    Console.WriteLine("Prüfsumme der Beweismitteldatei (MD5): " + HashMD5Image);
                }
            }

            // find HDD Model 
            findString = "Drive Model:";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HDDModel);
                Console.WriteLine("Drive Model: " + HDDModel);
            }

            // find size in GB and size in sectors
            // sector count is present two times in this type of log file 
            // once in the form of 1,251,223 and once with the format 1251223
            // we take the first format.
            findString = "Sector Count";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HDDSizeInSectors);
                if (line.Contains(","))
                {
                    HDDSizeInSectors = HDDSizeInSectors.Replace(',', '.').Trim();
                }
                Console.WriteLine("Anzahl von Sektoren: " + HDDSizeInSectors);

                NumberOfLBABlocks = GetNumberOfLBABlocks(HDDSizeInSectors, 512);
                Console.WriteLine("Anzahl der LBA Blöcke: " + NumberOfLBABlocks);

                HDDSizeInBytes = GetHDDSizeInBytesFromSectorCount(HDDSizeInSectors, 512);
                Console.WriteLine("HDDSizeInBytes: " + HDDSizeInBytes);
            }

            // find size in GB 
            findString = "Source data size";
            if (line.IndexOf(findString) != -1)
            {
                extractInformationFromKeywordUsingDelimter(findString, line, _delimiter, out HDDSizeInGB);
                Console.WriteLine("Größe in GB: " + HDDSizeInGB);
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_logFileContent"></param>
    /// <param name="_logType"></param>
    /// <returns></returns>
    public static int parseData(string _logFileContent, int _logType)
    {

        // early out
        if (_logType == AcquisitionLogFileReader.UNKOWNLOGTYPE)
        {
            Console.WriteLine("Error reading file..");
            return AcquisitionLogFileReader.UNKOWNLOGTYPE;
        }

        if (_logType == AcquisitionLogFileReader.TD3LOGTYPE)
        {
            parseTD3Log(_logFileContent);
        }

        if (_logType == AcquisitionLogFileReader.TD1LOGTYPE)
        {
            parseTD1Log(_logFileContent);
        }

        if (_logType == AcquisitionLogFileReader.FTKIMAGERLOGTYPE)
        {
            parseFTKLog(_logFileContent);
        }

        if (_logType == AcquisitionLogFileReader.FTKIMAGERLOGTYPEGERMAN)
        {
            parseFTKGermanLog(_logFileContent);
        }

        if (_logType == AcquisitionLogFileReader.XWAYSFORENSICLOGTYPE)
        {
            parseXWAYSLog(_logFileContent);
        }

        if (_logType == AcquisitionLogFileReader.MAGNETACQUIRELOGTYPE)
        {
            parseMagnetAcquireLog(_logFileContent);
            //Console.WriteLine("Not yet implemented.");
            //return AcquisitionLogFileReader.UNKOWNLOGTYPE;
        }

        // return success
        return 0;
    }

    public static int readFromFile(string _path)
    {
        // Open the text file using a stream reader.
        try
        {
            int type = 0;
            String content = String.Empty;
            bool changeEncodingToGerman = false;

            using (StreamReader sr = new StreamReader(_path))
            {
                // Read the first line of the stream to a string and determine the Log Type:
                content = sr.ReadToEnd();
                type = determineLogType(content, _path);

                if (type == AcquisitionLogFileReader.UNKOWNLOGTYPE)
                {
                    Console.WriteLine("The file " + _path + " could not be read due to unknown log file type.");
                }
                if (type == AcquisitionLogFileReader.XWAYSFORENSICLOGTYPE)
                {
                    changeEncodingToGerman = true;
                }
            }

            // We actually have to do this because of german umlauts in XWF-Logs
            if (changeEncodingToGerman)
            {
                //Console.WriteLine("We try something with Encodings....");
                using (StreamReader sr = new StreamReader(_path, System.Text.Encoding.UTF7))
                {
                    content = sr.ReadToEnd();
                    parseData(content, type);
                    LogFileContent = content;
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader(_path))
                {
                    content = sr.ReadToEnd();
                    parseData(content, type);
                    LogFileContent = content;
                }
            }

        }
        catch (Exception e)
        {
            Console.WriteLine("The file " + _path + " could not be read:");
            Console.WriteLine(e.Message);
            return -1;
        }
        return 0;
    }

}// end acquisitionlogfilereader


namespace AcquisitionLogParser
{

    class Program
    {
        #region Members
        public static Word.Application application;
        public static Word.Document docx;
        //  refs for traveling thru the word document
        public static object _missing = System.Reflection.Missing.Value;
        public static object _EOFF_Flag = "\\endofdoc"; /* \endofdoc is a predefined bookmark */
        #endregion

        /// <summary>
        /// Method to replace <Placeholder> with actual Text
        /// </summary>
        /// <param name="application">Word Application</param>
        /// <param name="findText">Placeholdertext</param>
        /// <param name="replacementText">Text to replace the placeholder</param>
        private static void findAndReplace(Word.Application application, object findText, object replacementText)
        {
            object matchCase = true;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundLike = false;
            object nmatchAllForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiactitics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;

            // default
            if ((string)replacementText == String.Empty)
            {
                replacementText = "n/a";
            }

            application.Selection.Find.Execute(ref findText,
                        ref matchCase, ref matchWholeWord,
                        ref matchWildCards, ref matchSoundLike,
                        ref nmatchAllForms, ref forward,
                        ref wrap, ref format, ref replacementText,
                        ref replace, ref matchKashida,
                        ref matchDiactitics, ref matchAlefHamza,
                        ref matchControl);
        }

        public static void printUsage()
        {
            Console.WriteLine("AcquisitionLogParser.exe WordTemplate.docx LogFile1 LogFile2 .... LogFilen");
            Console.WriteLine();
            Console.ReadKey();
        }

        static int Main(string[] args)
        {
            // early out
            if (args.Length == 0 || args.Length < 2)
            {
                Console.WriteLine("ERROR: too few arguments.");
                printUsage();
                return 1;
            }

            // create an acquisition
            //Acquisition forensicAcquisition = new Acquisition();
            //forensicAcquisition.InternalProjectName
            //forensicAcquisition.NameOfHWWriteBlocker
            //forensicAcquisition.SofwareVersion
            //forensicAcquisition.StartOfAcquisition

            // here begins all the black magicks
            //for (int i = 1; i < args.Length; i++)
            //{

            int status = AcquisitionLogFileReader.readFromFile(args[0]);

            if (status != 0)
            {
                Console.WriteLine("Error reading file.");
                Console.WriteLine("Exiting.");
                return 1;
            }
            //}
            else
            {
                try
                {
                    // Get a List of current processes
                    List<int> processesbeforegen = getRunningProcesses();

                    // open word
                    application = new Word.Application();// { Visible = true };

                    // create new document in Word.
                    //docx = application.Documents.Add(ref _missing, ref _missing, ref _missing, ref _missing);
                    Object _template = args[1];// @"C:\Users\sven\Documents\MeinForensikBericht.docx";
                    Object _readonly = false;

                    // only open files that are existing 
                    if (File.Exists((string)_template))
                    {

                        docx = application.Documents.Open(_template, _missing, _readonly);

                        docx.Activate();
                        docx.Content.SetRange(0, 0);

                        #region ReplaceText

                        findAndReplace(application, "<StartOfAcquisition>", AcquisitionLogFileReader.StartOfAcquisition);
                        findAndReplace(application, "<StartOfAcquisitionWOTime>", AcquisitionLogFileReader.StartOfAcquisitionWOTime);
                        findAndReplace(application, "<EndOfAcquisition>", AcquisitionLogFileReader.EndOfAcquisition);

                        findAndReplace(application, "<Examiner>", AcquisitionLogFileReader.Examiner);
                        findAndReplace(application, "<InternalProjectName>", AcquisitionLogFileReader.InternalProjectName);
                        findAndReplace(application, "<EvidenceID>", AcquisitionLogFileReader.EvidenceID);
                        findAndReplace(application, "<EvidenceIDShort>", AcquisitionLogFileReader.EvidenceIDShort);
                        findAndReplace(application, "<EvidenceIDWOHDD>", AcquisitionLogFileReader.EvidenceIDWOHDD);
                        findAndReplace(application, "<SoftwareVersion>", AcquisitionLogFileReader.SofwareVersion);

                        findAndReplace(application, "<HDDModel>", AcquisitionLogFileReader.HDDModel);
                        findAndReplace(application, "<HDDSerialNumber>", AcquisitionLogFileReader.HDDSerialNumber);
                        findAndReplace(application, "<HDDSizeInGB>", AcquisitionLogFileReader.HDDSizeInGB);
                        findAndReplace(application, "<HDDSizeInSectors>", AcquisitionLogFileReader.HDDSizeInSectors);
                        findAndReplace(application, "<HDDSizeInBytes>", (AcquisitionLogFileReader.HDDSizeInBytes));
                        findAndReplace(application, "<NumberOfLBABlocks>", AcquisitionLogFileReader.NumberOfLBABlocks);
                        findAndReplace(application, "<HWInterface>", AcquisitionLogFileReader.HWInterface);
                        findAndReplace(application, "<NameOfHWWriteBlocker>", AcquisitionLogFileReader.NameOfHWWriteBlocker);

                        findAndReplace(application, "<ImageSizeInGB>", AcquisitionLogFileReader.ImageSizeInGB);

                        findAndReplace(application, "<Partition1FS>", AcquisitionLogFileReader.Partition1FileSystem);
                        findAndReplace(application, "<Partition1SizeInBytes>", AcquisitionLogFileReader.Partition1SizeInBytes);
                        findAndReplace(application, "<Partition2FS>", AcquisitionLogFileReader.Partition2FileSystem);
                        findAndReplace(application, "<Partition2SizeInBytes>", AcquisitionLogFileReader.Partition2SizeInBytes);

                        findAndReplace(application, "<HashMD5>", AcquisitionLogFileReader.HashMD5);
                        findAndReplace(application, "<HashSHA1>", AcquisitionLogFileReader.HashSHA1);

                        findAndReplace(application, "<HashSHA1Image>", AcquisitionLogFileReader.HashSHA1Image);
                        findAndReplace(application, "<HashMD5Image>", AcquisitionLogFileReader.HashMD5Image);

                        // Limit of find and replace (254 characters) ..
                        // seen here: http://www.wordbanter.com/showthread.php?t=136920
                        //findAndReplace(application, "<LogFileContent>", AcquisitionLogFileReader.LogFileContent.Substring(0, 254));
                        #endregion


                        #region WriteStuff2Doc
                        /*
                        //Add heading
                        Word.Paragraph para1 = docx.Content.Paragraphs.Add(ref _missing);

                        para1.Range.Text = "Datensicherung";
                        Object styleHeading1 = "Überschrift 1"; // 
                        para1.Range.set_Style(ref styleHeading1);
                        para1.Range.InsertParagraphAfter();


                        //Add paragraph..
                        Word.Paragraph para2 = docx.Content.Paragraphs.Add(ref _missing);
                        para2.Range.Text = "Die Festplatte wurde von uns am " + AcquisitionLogFileReader.StartOfAcquisition + " forensisch gesichert. " +
                            "Dazu wurde die Festplatte aus dem System ausgebaut und an den forensischen Duplikator " + AcquisitionLogFileReader.NameOfHWWriteBlocker + " anschlossen. \n";
                        para2.Range.InsertParagraphAfter();

                        //create table object
                        Word.Table _table;

                        //add table object in word document with 2 rows and 4 columns
                        _table = docx.Tables.Add(docx.Bookmarks.get_Item(ref _EOFF_Flag).Range, 2, 4, ref _missing, ref _missing);
                        // dunno yet:
                        _table.Range.ParagraphFormat.SpaceAfter = 6;

                        _table.Range.InsertCaption(Word.WdCaptionLabelID.wdCaptionTable, ": Übersicht der Datensicherungen");
                        //_table.Title = "test..";

                        // neckbreaker
                        _table.set_Style("Gitternetztabelle 4");

                        //fill table:
                        _table.Cell(1, 1).Range.Text = "Datensicherung";
                        _table.Cell(1, 2).Range.Text = "Start";
                        _table.Cell(1, 3).Range.Text = "Ende";
                        _table.Cell(1, 4).Range.Text = "Prüfsumme";

                        _table.Cell(2, 1).Range.Text = "n/a";
                        _table.Cell(2, 2).Range.Text = AcquisitionLogFileReader.StartOfAcquisition;
                        _table.Cell(2, 3).Range.Text = AcquisitionLogFileReader.EndOfAcquisition;
                        _table.Cell(2, 4).Range.Text = AcquisitionLogFileReader.HashMD5;

                        //make first row of table BOLD
                        _table.Rows[1].Range.Font.Bold = 1;
                        //_table.PreferredWidth = 100;
                        //_table.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;


                        //Add paragraph.. moar text
                        Word.Paragraph para3 = docx.Content.Paragraphs.Add(docx.Bookmarks.get_Item(ref _EOFF_Flag).Range);
                        para3.Range.Text = "\nIm Folgenden sind die technischen Details zum gesicherten Datenträger beschrieben.";
                        para3.Range.InsertParagraphAfter();

                        Word.Paragraph p3 = docx.Content.Paragraphs.Add(docx.Bookmarks.get_Item(ref _EOFF_Flag).Range);
                        p3.Range.Text = "Datum der Sicherung: " + AcquisitionLogFileReader.StartOfAcquisition + "\n" +
                                        "Schreibschutz: " + AcquisitionLogFileReader.NameOfHWWriteBlocker + "\n" +
                                        "Seriennummer: " + AcquisitionLogFileReader.HDDSerialNumber + "\n" +
                                        "Model: " + AcquisitionLogFileReader.HDDModel + "\n" +
                                        "Größe: " + AcquisitionLogFileReader.HDDSizeInGB + "\n" +
                                        "Speicherkapazität in Sektoren: " + AcquisitionLogFileReader.HDDSizeInSectors + "\n" +
                                        "Anschluss: " + AcquisitionLogFileReader.HWInterface + "\n" +
                                        "Prüfsumme:" + AcquisitionLogFileReader.HashMD5 + "\n";
                        */

                        // Add a picture to the current Cursor position (range)
                        //docx.InlineShapes.AddPicture(@"C:\SamplePicture.png");
                        #endregion

                        // save work and kill processes
                        saveWordFile("test.docx", processesbeforegen);
                    }
                    else
                    {
                        Console.WriteLine("Error openening template/word document");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("There was an error :/");
                    Console.WriteLine(e.Message.ToString());
                }

            }
            // Keep the console window open
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();

            return 0;
        }

        private static List<int> getRunningProcesses()
        {
            List<int> ProcessIDs = new List<int>();
            //here we're going to get a list of all running processes on
            //the computer
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (Process.GetCurrentProcess().Id == clsProcess.Id)
                    continue;
                if (clsProcess.ProcessName.Contains("WINWORD"))
                {
                    ProcessIDs.Add(clsProcess.Id);
                }
            }
            return ProcessIDs;
        }

        public static void saveWordFile(string _path, List<int> processesbeforegen)
        {
            object szPath = _path;
            Console.WriteLine("Saving Word Document as " + _path);
            docx.SaveAs(ref szPath);
            docx.Close(Word.WdSaveOptions.wdDoNotSaveChanges);
            application.Quit();

            // ensure that processes are killed
            List<int> processesaftergen = getRunningProcesses();
            killProcesses(processesbeforegen, processesaftergen);

        }

        private static void killProcesses(List<int> processesbeforegen, List<int> processesaftergen)
        {
            foreach (int pidafter in processesaftergen)
            {
                bool processfound = false;
                foreach (int pidbefore in processesbeforegen)
                {
                    if (pidafter == pidbefore)
                    {
                        processfound = true;
                    }
                }

                if (processfound == false)
                {
                    Process clsProcess = Process.GetProcessById(pidafter);
                    clsProcess.Kill();
                }
            }
        }

    } // end Program
} // end Namespace