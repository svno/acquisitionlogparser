
--------------------------------Disk Information--------------------------------

Vendor: 
Model: ST3250824AS
Revision: 3.AHH
Serial Number: 9ND15WV5
Bus: SATA
Device: Direct Access
Capacity: 250.0 GB (250,059,350,016 bytes)
Removable Media: No
Cylinders: 30401
Tracks per Cylinder: 255
Sector per Track: 63
Bytes per Sector: 512

-------------------------------Bridge Information-------------------------------

Vendor: Tableau
Model: T35es
Description: Forensic SATA/IDE Bridge
Serial number: 000ecc20 0035007a
Channel: SATA
Bridge Access Mode: Read-Only
Read-Only Declaration: Suppresses Read-Only
Write Error Declaration: Suppresses Write Errors
Firmware stepping: 5
Firmware build date: Jan 23 2013
Firmware build time: 12:20:26
Firmware build type: Release
Drive Vendor: 
Drive Model: ST3250824AS
Drive Serial Number: 9ND15WV5
Drive Revision: 3.AHH

------------------------------HPA/DCO Information-------------------------------

HPA Supported: No
HPA in Use: No
DCO Supported: Yes
DCO in Use: No
Security Supported: No
Security in Use: No
Reported Capacity: 250.0 GB (250,059,350,016 bytes)
HPA Capacity: 250.0 GB (250,059,350,016 bytes)
DCO Capacity: 250.0 GB (250,059,350,016 bytes)
