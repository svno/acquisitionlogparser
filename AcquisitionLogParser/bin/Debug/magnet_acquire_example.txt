Imager Product: Magnet ACQUIRE
Imager Version: 2.0.0.0699

Examiner Name: SOS
Evidence Number: ASS02_USB01
Description: USB Stick hama

Relative Activity Log Path: activity_log.txt
Original Activity Log Path: C:\Users\sven\Desktop\USB-Image\activity_log.txt
Activity Log MD5 Hash: 864EB24660F93EAAD9C944F07C2AD21E

Output Directory: USB-Image
Full Output Directory: C:\Users\sven\Desktop\USB-Image

Total Segments: 1

Relative Segment 1 Path: Generic Flash Disk USB Device 7,47 GB full image.raw
Full Segment 1 Path: C:\Users\sven\Desktop\USB-Image\Generic Flash Disk USB Device 7,47 GB full image.raw
Segment 1 MD5 Hash: D7AE99CB4CC9A5041053E88FB941FBAD
Segment 1 SHA1 Hash: 9621A3FFDD82E49637A6B38BC098FDC3843C15EB

Imaging Start UTC: 2016-07-04 19:55:52
Imaging Start UTC Ticks: 636032589529602878
Imaging End UTC: 2016-07-04 20:08:08
Imaging End UTC Ticks: 636032596885943637

Device Information
Manufacturer: Generic Flash Disk USB Device
Product Model: Generic Flash Disk USB Device
Operating System Version: 
Unique Identifier: 
Serial Number: 1

Additional Device Information
Device Name: PhysicalDrive1
Pretty Name: PhysicalDrive1 Generic Flash Disk USB Device (7,47 GB)
Device ID: 1
Serial Number: 
Model: Generic Flash Disk USB Device
Media Type: Removable Media
Size: 8019509248
Device Size: 7,47 GB
Bytes Per Sector: 512
Sectors Per Track: 63
Tracks Per Cylinder: 255
Total Sectors: 15647310
Can Search File System: True
Has Encrypted Volumes: False
Image Summary: 
Disk Geometry
    Sector Size	: 512
    Total Sectors	: 15647310
    Media Size	: 8011422720

Partitions
    1	: IFS (NTFS or HPFS) (Start LBA: 4096, End LBA: 15661055 - Total Sectors: 15656960)

